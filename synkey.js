$(function() {
  synifyAll(".synify");
  if($("#syntext").length > 0) {
  $(document).keypress(function(e) {
    var c = String.fromCharCode(e.which);
    var syntext = $("#syntext").get(0);
    if(e.which === 8) {
      $("#syntext").children().last().remove();
    }
    else if(e.which === 13) {
      syntext.innerHTML += synifyChar("\n");
    }
    else {
      syntext.innerHTML += synifyChar(c);
    }
    e.preventDefault();
  });
  }
});

function synifyAll(selector) {
  var elements = $(selector).get();
  for (var i = 0; i < elements.length; i++) {
    synify(elements[i]);
  }
}

function synify(element) {
  var text = element.innerText;
  var html = "";
  for(var i = 0; i < text.length; i++) {
    html += synifyChar(text[i]);
  }
  element.innerHTML = html;
}

function synifyChar(c) {
  if(c.match(/[a-zA-Z]/)){
    return "<span class='char " + c + "'>" + c + "</span>";
  }
  else if(c.match(/[0-9]/)){
    return "<span class='char n" + c + "'>" + c + "</span>";
  }
  else if(c === " ") {
    return "<span class='char space'>&nbsp;</span>";
  }
  else if(c === "\n") {
    return "<br>"
  }
  else { 
    return "<span class='char other'>" + c + "</span>";
  }
}

